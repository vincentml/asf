# ASF Sharing Policy Generator

This tool generates sharing policies for the [Article Sharing Framework](https://www.stm-assoc.org/asf/). Each sharing policy created using this tool is a machine-actionable representations of a publishers' policy for sharing protected content.

Each sharing policy is represented in multiple formats:

* as human-readable text
* as machine-actionable metadata
* as a DOI persistent identifier
* as a URL (which the DOI points to)
* as a table visualization
* as an abbreviation

This tool generates HTML that includes all of these formats. The sharing policies are created using a configuration file.

## Sharing Policies

[Click here](https://vincentml.gitlab.io/asf/) to see the list of sharing policies.

Sharing policies that are numbered 1 - 48 are permutations of the four facets that are described in the Article Sharing Framework: Platform Type, Audience Scope, Journal Article Version, and Displayable Element.

Sharing policies that are numbered 49 and above are custom combinations of the four facets. **Each of these sharing policies is a representation of a publishers' actual sharing policy or a sharing policy that is likely to be in use by some publisher. New sharing policies with custom combinations can easily be added upon request.**

Sharing policy 999 is only for testing. It provides an example of how a sharing policy that has been updated can have links between older versions and newer versions.

## Using sharing policies

### Publishers

Publishers can add the DOI of their preferred sharing policy to their Crossref article metadata records. The sharing policy DOI should be added in a license_ref element as a DOI URL. For example:

    <ali:license_ref applies_to="asf">https://doi.org/10.15223/policy-001</ali:license_ref>

### Platforms

Platforms can use the DOI of a published article to retrieve the published article's metadata using the Crossref API. The platform can then check the article metadata for the presence of a sharing policy DOIs that it recognizes, or can retrieve the machine-actionable sharing policy metadata from the sharing policy DOI URL. This provides the platform with actionable information about whether, and under what conditions, a published article can be shared.


## Sharing policy metadata

Sharing policies are represented in machine-actionable format as JSON data. The JSON data represents the permissions of a sharing policy using a standard Linked Data vocabulary [Open Digital Rights Language 2.2](https://www.w3.org/TR/odrl-model/) and a specific vocabulary *asf-vocabulary.jsonld* that provides terms for the Article Sharing Framework.

The sharing policy's metadata is contained in a **&lt;script type="application/ld+json">** element in the &lt;body> of the HTML.

Within the JSON data, the **permission** array contains one or more permissions that are provided by the sharing policy. Each permission describes a condition under which protected content can be shared. The conditions use the four facets that are described in the Article Sharing Framework. These four facets are shown in this table.

<table>
<thead>
  <tr>
    <th>Facet Name</th>
    <th>Facet Key</th>
    <th>Possible Values</th>
    <th>Value Name</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td rowspan="4">Displayable Element</td>
    <td rowspan="4">asf:displayable_element</td>
    <td>asf:full_text</td>
    <td>Full Text</td>
  </tr>
  <tr>
    <td>asf:citation</td>
    <td>Citation Metadata</td>
  </tr>
  <tr>
    <td>asf:abstract</td>
    <td>Abstract</td>
  </tr>
  <tr>
    <td>asf:reference_list</td>
    <td>Reference List</td>
  </tr>
  <tr>
    <td rowspan="3">Journal Article Version</td>
    <td rowspan="3">jav:journal_article_version</td>
    <td>jav:VoR</td>
    <td>Version of Record</td>
  </tr>
  <tr>
    <td>jav:AM</td>
    <td>Accepted Manuscript</td>
  </tr>
  <tr>
    <td>jav:AO</td>
    <td>Author Original</td>
  </tr>
  <tr>
    <td rowspan="2">Audience Scope</td>
    <td rowspan="2">asf:audience</td>
    <td>asf:research_collaboration_group</td>
    <td>Research Collaboration Group</td>
  </tr>
  <tr>
    <td>asf:general_access</td>
    <td>General Access</td>
  </tr>
  <tr>
    <td rowspan="2">Platform Type</td>
    <td rowspan="2">asf:platform</td>
    <td>asf:platform_signed</td>
    <td>Platform Signed</td>
  </tr>
  <tr>
    <td>asf:platform_not_signed</td>
    <td>Platform Not Signed</td>
  </tr>
</tbody>
</table>

## Example

This example is based on sharing policy 5, which permits the full text (i.e. a PDF) of an article that was published in a journal to be shared with a research collaboration group on any type of platform. The sharing policy describes this permission with the following text:

"Any platform, regardless of whether it has signed and complies with the STM Voluntary Principles for Article Sharing, can allow the sharing of the Full Text, including Abstract, References, and Citation Metadata of the Version of Record in Research Collaboration Groups."

and with the following metadata in JSON format:

```json
"permission":[
    {"action":"distribute", "target":"schema:ScholarlyArticle",
    "constraint":[
        {"leftOperand":"asf:platform","operator":"isAnyOf","rightOperand":[
            {"@id":"asf:platform_signed"},
            {"@id":"asf:platform_not_signed"}]},
        {"leftOperand":"asf:audience","operator":"isAnyOf","rightOperand":[
            {"@id":"asf:research_collaboration_group"}]},
        {"leftOperand":"jav:journal_article_version","operator":"isAnyOf","rightOperand":[
            {"@id":"jav:VoR"}]},
        {"leftOperand":"asf:displayable_element","operator":"isAnyOf","rightOperand":[
            {"@id":"asf:full_text"},
            {"@id":"asf:reference_list"},
            {"@id":"asf:abstract"},
            {"@id":"asf:citation"}]}
    ]}]
```

## How to create sharing policies

Sharing policies for specific combinations of the four facets can be added by editing the **policies.xml** configuration file.

The policies.xml configuration file can be edited using any good text editing software. The It is recommended to use editing software that can validate XML using XML Schema and Schematron (such as [oXygen XML](http://oxygenxml.com/)) to get automatic checking for errors and editing assistance. Do not use a word processor (such as Microsoft Word) because word processors create unintended changes in XML files. Before starting, clone the repository from GitLab to your computer so that you can edit policies.xml. Alternatively, [click here](https://gitlab.com/-/ide/project/vincentml/asf/tree/master/-/policies.xml/) to edit policies.xml online.

*Step 1:* Add a new sharing policy to the configuration file

1. Open the policies.xml file in the text editing software of your choice.

2. Add a new &lt;policy> element after the last &lt;/policy> tag.

3. Give the policy a new policy number in the *number* attribute, and add attribute policy_version="1.0".

4. Then add as many &lt;permit> elements as are needed to express the combination of permissions.

5. Save the file

*Step 2:* Preview changes

1. Run *gradlew.bat* (if on Windows) or *gradlew* (if on Mac or Linux)
   * The generator will run and should finish with a message "Generating policy web pages BUILD SUCCESSFUL".
   * If the generator finishes with a message "BUILD FAILED" look through the messages that are displayed to find out what went wrong.

2. Look in the **public** folder and open **index.html** in a web browser.

*Step 3:* Make the sharing policy public

1. Commit the changes you made to policies.xml to the Git repository and push the changes to GitLab.
   * After a few minutes the new sharing policy should be available at https://vincentml.gitlab.io/asf/
   * If the update does not happen check the [CI/CD](https://gitlab.com/vincentml/asf/-/pipelines) page to find out what went wrong.

2. Copy the HTML for the new sharing policy to a new page on stm-assoc.org. Be sure to:
   1. Use the URL that was generated
   2. View the HTML source to copy the tags not just what is visible.
   3. Copy everything that is inside the &lt;article> tag, including the &lt;p> tags that contain the human-readable text and the &lt;script type="application/ld+json"> tag that contains machine-actionable metadata.
   4. In the WordPress page editor, click on "Text" to switch from visual mode into text mode and then paste.  

3. Register the sharing policy's DOI with Crossref so that the DOI will redirect to the URL.
   1. Download crossref.xml from the Crossref DOIs page.
   2. Log in to https://doi.crossref.org/
   3. Navigate to the Submissions, Upload page.
   4. Select crossref.xml, type 'Metadata', and then click the 'upload' button.
