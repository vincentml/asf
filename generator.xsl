<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:asf="https://doi.org/10.15223/asf-vocabulary#"
    xmlns:html="http://www.w3.org/1999/xhtml"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="xs asf"
    expand-text="yes"
    version="3.0">
    
    <!-- ** destination folder to save files is calculated to a 'public' folder in the folder where this XSLT resides -->
    <xsl:param name="destination" as="xs:string" select="static-base-uri() => replace('/[^/]*$', '') => concat('/public/')"/>
    
    <!-- ** published/modified date for the policies is calcuated to today -->
    <xsl:param name="modified_date" as="xs:date" select="adjust-date-to-timezone(current-date(), xs:dayTimeDuration('PT0H'))"/>

    <!-- ** serialization configuration to produce HTML 5 -->
    <xsl:output name="html5" method="xhtml" html-version="5.0" encoding="UTF-8" indent="yes"/>
    
    <!-- STM DOI Prefix -->
    <xsl:variable name="doi-prefix" select="'https://doi.org/10.15223/'"/>
    
    <!-- STM ASF URL base -->
    <xsl:variable name="url-prefix" select="'https://www.stm-assoc.org/asf/'"/>
    
    <!-- STM ASF DOI -->
    <xsl:variable name="asf-doi" select="$doi-prefix || 'asf'"/>
    
    <!-- ** Vocabulary for JSON-LD DOI -->
    <xsl:variable name="vocab-doi" select="$doi-prefix || 'asf-vocabulary'"/>
    
    <!-- ** Vocabulary for JSON-LD URL -->
    <xsl:variable name="vocab-url" select="'https://www.stm-assoc.org/asf/asf-vocabulary.jsonld'"/>
    <!-- <xsl:variable name="vocab-url" select="'https://vincentml.gitlab.io/asf/asf-vocabulary.jsonld'"/> -->
    
    <!-- ** ODRL Profile DOI -->
    <xsl:variable name="odrl-profile-doi" select="$doi-prefix || 'asf-profile'"/>
    
    <!-- ** platforms -->
    <xsl:variable name="ps" select="xs:NMTOKEN('ps')"/>
    <xsl:variable name="pns" select="xs:NMTOKEN('pns')"/>
    <!-- ** journal article versions -->
    <xsl:variable name="vor" select="xs:NMTOKEN('vor')"/>
    <xsl:variable name="am" select="xs:NMTOKEN('am')"/>
    <xsl:variable name="ao" select="xs:NMTOKEN('ao')"/>
    <!-- ** audience scope -->
    <xsl:variable name="ga" select="xs:NMTOKEN('ga')"/>
    <xsl:variable name="rcg" select="xs:NMTOKEN('rcg')"/>
    <!-- ** displayable elements -->
    <xsl:variable name="cm" select="xs:NMTOKEN('cm')"/>
    <xsl:variable name="ab" select="xs:NMTOKEN('ab')"/>
    <xsl:variable name="ref" select="xs:NMTOKEN('ref')"/>
    <xsl:variable name="ft" select="xs:NMTOKEN('ft')"/>
    
    <!-- ** run this XSLT with no input XML -->
    <xsl:template name="xsl:initial-template">
        <xsl:call-template name="asf:generate-pages">
            <xsl:with-param name="policies" select="(asf:permutations(), $policy_example)"/>
        </xsl:call-template>
    </xsl:template>
    
    <!-- ** run this XSLT with input XML that adheres to policies.xsd -->
    <xsl:template match="asf:policies">
        <xsl:call-template name="asf:generate-pages">
            <xsl:with-param name="policies" select="(asf:permutations(), asf:policy)"/>
            <xsl:with-param name="subsets" select="asf:subset"/>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:variable name="policy_example" as="element(asf:policy)*">
        <asf:policy number="100" policy_version="1.0" replaces_version="0.1" replaced_by_version="2.0">
            <asf:permit platform="pns" jav="vor" audience="ga" element="ft"/>
            <asf:permit platform="pns" jav="am" audience="ga" element="ft"/>
        </asf:policy>
    </xsl:variable>
    
    <xsl:function name="asf:permutations" as="element(asf:policy)*">
        <!-- ** generate all 48 permutations in the same order as Dan's spreadsheet -->
        <xsl:variable name="policies" as="element(asf:policies)">
            <asf:policies>
                <xsl:iterate select="($pns, $ps)">
                    <xsl:variable name="platform" select="."/>
                    <xsl:iterate select="($vor, $am, $ao)">
                        <xsl:variable name="jav" select="."/>
                        <xsl:iterate select="($ga, $rcg)">
                            <xsl:variable name="audience" select="."/>
                            <xsl:iterate select="($ft, $ab, $ref, $cm)">
                                <xsl:variable name="element" select="."/>
                                <asf:policy policy_version="1.0">
                                    <asf:permit platform="{$platform}" element="{$element}" jav="{$jav}" audience="{$audience}"/>
                                </asf:policy>
                            </xsl:iterate>
                        </xsl:iterate>
                    </xsl:iterate>
                </xsl:iterate>
            </asf:policies>
        </xsl:variable>
        <!-- ** number the policies -->
        <xsl:for-each select="$policies/asf:policy">
            <xsl:copy>
                <xsl:attribute name="number">
                    <xsl:number/>
                </xsl:attribute>
                <xsl:copy-of select="@*, *"/>
            </xsl:copy>
        </xsl:for-each>
    </xsl:function>
    
    <!-- ** generate English text for a combination of platform, jav, audience, and displayable element -->
    <xsl:function name="asf:policy-text" as="xs:string">
        <xsl:param name="platform" as="xs:string"/>
        <xsl:param name="jav" as="xs:string"/>
        <xsl:param name="audience" as="xs:string"/>
        <xsl:param name="element" as="xs:string"/>
        <xsl:variable name="templates" as="map(xs:string, text())*">
            <xsl:map>
                <xsl:map-entry key="$pns">Any platform, regardless of whether it has signed and complies with the STM Voluntary Principles for Article Sharing, can:</xsl:map-entry>
                <xsl:map-entry key="$ps">A platform that has signed and is compliant with the STM Voluntary Principles for Article Sharing can:</xsl:map-entry>
                <xsl:map-entry key="$ft">allow the sharing of the Full Text, including Abstract, References, and Citation Metadata</xsl:map-entry>
                <xsl:map-entry key="$ft||$vor">allow the sharing of the Full-Text Version of Record, including Abstract, References, and Citation Metadata</xsl:map-entry>
                <xsl:map-entry key="$ft||$am">allow the sharing of the Full-Text Accepted Manuscript, including Abstract, References, and Citation Metadata</xsl:map-entry>
                <xsl:map-entry key="$ft||$ao">allow the sharing of the Full-Text Author Original, including Abstract, References, and Citation Metadata</xsl:map-entry>
                <xsl:map-entry key="$ab">allow the sharing of the Abstract and Citation Metadata</xsl:map-entry>
                <xsl:map-entry key="$ref">allow the sharing of the References and Citation Metadata</xsl:map-entry>
                <xsl:map-entry key="$cm">allow only the sharing of the Citation Metadata</xsl:map-entry>
                <xsl:map-entry key="$vor||$am||$ao">of the Version of Record, Accepted Manuscript, or Author Original</xsl:map-entry>
                <xsl:map-entry key="$vor">of the Version of Record</xsl:map-entry>
                <xsl:map-entry key="$am">of the Accepted Manuscript</xsl:map-entry>
                <xsl:map-entry key="$ao">of the Author Original</xsl:map-entry>
                <xsl:map-entry key="$ga">for General Access, including any Research Collaboration Groups</xsl:map-entry>
                <xsl:map-entry key="$rcg">in Research Collaboration Groups</xsl:map-entry>
            </xsl:map>
        </xsl:variable>
        <xsl:variable name="parts" as="text()+" select="if ($element eq $ft) 
            then ($templates($platform), $templates($element||$jav), $templates($audience))
            else ($templates($platform), $templates($element), $templates($jav), $templates($audience))"/>
        <xsl:sequence select="string-join($parts, ' ') || '.'"/>
    </xsl:function>
    
    <!-- ** generate English text for a policy -->
    <xsl:function name="asf:policy-statement" as="element(html:p)+">
        <xsl:param name="policy" as="element(asf:policy)"/>
        <xsl:for-each select="$policy/asf:permit">
            <p><xsl:value-of select="asf:policy-text(@platform, @jav, @audience, @element)"/></p>
        </xsl:for-each>
        
        <!-- ** ensure the text explicitly allows sharing citation metadata to general audience on all platforms -->
        <!-- turning off automatic permission for public citation metdata
        <xsl:if test="not(asf:permits-citation($policy))">
            <p><xsl:value-of select="asf:policy-text($pns, $vor||$am||$ao, $ga, $cm)"/></p>
        </xsl:if>
        -->
    </xsl:function>
    
    <!-- ** detect if the policy allows sharing citation metadata to general audience on all platforms
            to find out if an assertion should be added to explictly allow publicly sharing citation metadata. -->
    <xsl:function name="asf:permits-citation" as="xs:boolean">
        <xsl:param name="policy" as="element(asf:policy)"/>
        <xsl:sequence select="
            (some $p in ($policy/asf:permit) satisfies (
            $p/@platform = $pns and $p/@jav = $vor and $p/@audience = $ga))
            and (some $p in ($policy/asf:permit) satisfies (
            $p/@platform = $pns and $p/@jav = $am and $p/@audience = $ga))
            and (some $p in ($policy/asf:permit) satisfies (
            $p/@platform = $pns and $p/@jav = $ao and $p/@audience = $ga))"/>
    </xsl:function>
    
    <!-- ** create the DOI for a policy -->
    <xsl:function name="asf:policy-doi" as="xs:string">
        <xsl:param name="number" as="xs:integer"/>
        <xsl:sequence select="concat($doi-prefix, 'policy-', format-number($number, '000'))"/>
    </xsl:function>
    
    <!-- ** create the file name for a policy page -->
    <xsl:function name="asf:policy-filename" as="xs:string">
        <xsl:param name="number" as="xs:integer"/>
        <xsl:param name="policy_version" as="xs:string"/>
        <xsl:sequence select="concat('policy-', format-number($number, '000'), '-v', replace($policy_version, '\.', '-'), '.html')"/>
    </xsl:function>
    
    <!-- ** create anchor name for policy -->
    <xsl:function name="asf:policy-anchor" as="xs:NMTOKEN">
        <xsl:param name="number" as="xs:integer"/>
        <xsl:param name="policy_version" as="xs:string"/>
        <xsl:sequence select="xs:NMTOKEN(concat('policy-', format-number($number, '000'), '-v', replace($policy_version, '\.', '-')))"/>
    </xsl:function>
    
    <!-- ** create the URL for a policy page -->
    <xsl:function name="asf:policy-url" as="xs:string">
        <xsl:param name="number" as="xs:integer"/>
        <xsl:sequence select="concat($url-prefix, 'policy-', format-number($number, '000'), '/')"/>
    </xsl:function>
    
    <!-- ** create the title of a policy -->
    <xsl:function name="asf:policy-title" as="xs:string">
        <xsl:param name="number" as="xs:integer"/>
        <xsl:sequence select="concat('Article Sharing Framework Policy #', $number)"/>
    </xsl:function>
    
    <!-- ** create an abbreviation summarizing what is allowed by a policy -->
    <xsl:function name="asf:abbreviation" as="xs:string">
        <xsl:param name="policy" as="element(asf:policy)"/>
        <xsl:sequence select="string-join(($policy/asf:permit ! string-join((concat('#', asf:policy-number-for-permit(.)), @platform, @jav, @audience, @element), ' ')), ' and ')"/>
    </xsl:function>
    
    <!-- ** match an asf:permit to the 48 permutations and return the corresponding policy number -->
    <xsl:function name="asf:policy-number-for-permit" as="xs:string">
        <xsl:param name="permit" as="element(asf:permit)"/>
        <xsl:variable name="permutations" select="asf:permutations()"/>
        <xsl:sequence select="$permutations[deep-equal(asf:permit, $permit)]/@number"/>
    </xsl:function>
    
    <!-- ** generate metadata for a policy to embed in policy web pages.
            The metadata is in JSON-LD format and uses ODRL (a W3C Recommendation), 
            Dublin Core, Schema.org, and a vocabulary that provides the terms for ASF and JAV. 
            The DOI 'https://doi.org/10.15223/asf-vocabulary' should redirect to asf-vocabulary.jsonld
    -->
    <xsl:function name="asf:policy-metadata" as="xs:string">
        <xsl:param name="policy" as="element(asf:policy)"/>
        <xsl:variable name="data">
            <map xmlns="http://www.w3.org/2005/xpath-functions">
                <array key="@context">
                    <string>http://www.w3.org/ns/odrl.jsonld</string>
                    <string>{$vocab-doi}</string>
                </array>
                <string key="@type">Policy</string>
                <string key="profile">{$odrl-profile-doi}</string>
                <string key="uid">{asf:policy-doi($policy/@number)}</string>
                <string key="dc:title">{asf:policy-title($policy/@number)}</string>
                <string key="dc:modified">{format-date($modified_date, '[Y]-[M01]-[D01]')}</string>

                <xsl:if test="$policy/@replaces_version">
                    <string key="dc:replaces">{asf:policy-url($policy/@number)}</string>
                </xsl:if>
                <xsl:if test="$policy/@replaced_by_version">
                    <string key="dc:isReplacedBy">{asf:policy-url($policy/@number)}</string>
                </xsl:if>
                
                <string key="conflict">prohibit</string>
                <array key="permission">
                    <xsl:for-each select="$policy/asf:permit">
                        <map>
                            <string key="action">distribute</string>
                            <string key="target">schema:ScholarlyArticle</string>
                            <array key="constraint">
                                <map>
                                    <string key="leftOperand">asf:platform</string>
                                    <string key="operator">isAnyOf</string>
                                    <array key="rightOperand">
                                        <xsl:if test="@platform eq $ps">
                                            <map><string key="@id">asf:platform_signed</string></map>
                                        </xsl:if>
                                        <xsl:if test="@platform eq $pns">
                                            <map><string key="@id">asf:platform_signed</string></map>
                                            <map><string key="@id">asf:platform_not_signed</string></map>
                                        </xsl:if>
                                    </array>
                                </map>
                                <map>
                                    <string key="leftOperand">asf:audience</string>
                                    <string key="operator">isAnyOf</string>
                                    <array key="rightOperand">
                                        <xsl:if test="@audience eq $rcg">
                                            <map><string key="@id">asf:research_collaboration_group</string></map>
                                        </xsl:if>
                                        <xsl:if test="@audience eq $ga">
                                            <map><string key="@id">asf:research_collaboration_group</string></map>
                                            <map><string key="@id">asf:general_access</string></map>
                                        </xsl:if>
                                    </array>
                                </map>
                                <map>
                                    <string key="leftOperand">jav:journal_article_version</string>
                                    <string key="operator">isAnyOf</string>
                                    <array key="rightOperand">
                                        <xsl:if test="@jav eq $vor">
                                            <map><string key="@id">jav:VoR</string></map>
                                        </xsl:if>
                                        <xsl:if test="@jav eq $am">
                                            <map><string key="@id">jav:AM</string></map>
                                        </xsl:if>
                                        <xsl:if test="@jav eq $ao">
                                            <map><string key="@id">jav:AO</string></map>
                                        </xsl:if>
                                    </array>
                                </map>
                                <map>
                                    <string key="leftOperand">asf:displayable_element</string>
                                    <string key="operator">isAnyOf</string>
                                    <array key="rightOperand">
                                        <xsl:if test="@element eq $ft">
                                            <map><string key="@id">asf:full_text</string></map>
                                            <map><string key="@id">asf:reference_list</string></map>
                                            <map><string key="@id">asf:abstract</string></map>
                                            <map><string key="@id">asf:citation</string></map>
                                        </xsl:if>
                                        <xsl:if test="@element eq $ab">
                                            <map><string key="@id">asf:abstract</string></map>
                                            <map><string key="@id">asf:citation</string></map>
                                        </xsl:if>
                                        <xsl:if test="@element eq $ref">
                                            <map><string key="@id">asf:reference_list</string></map>
                                            <map><string key="@id">asf:citation</string></map>
                                        </xsl:if>
                                        <xsl:if test="@element eq $cm">
                                            <map><string key="@id">asf:citation</string></map>
                                        </xsl:if>
                                    </array>
                                </map>
                            </array>
                        </map>
                    </xsl:for-each>
                    <!-- ** ensure that the policy explicitly allows citation metadata to be shared publicly -->
                    <!-- turning off automatic permission for public citation metdata
                    <xsl:if test="not(asf:permits-citation($policy))">
                        <map>
                            <string key="action">distribute</string>
                            <string key="target">schema:ScholarlyArticle</string>
                            <array key="constraint">
                                <map>
                                    <string key="leftOperand">asf:platform</string>
                                    <string key="operator">isAnyOf</string>
                                    <array key="rightOperand">
                                        <map><string key="@id">asf:platform_signed</string></map>
                                        <map><string key="@id">asf:platform_not_signed</string></map>
                                    </array>
                                </map>
                                <map>
                                    <string key="leftOperand">asf:audience</string>
                                    <string key="operator">isAnyOf</string>
                                    <array key="rightOperand">
                                        <map><string key="@id">asf:research_collaboration_group</string></map>
                                        <map><string key="@id">asf:general_access</string></map>
                                    </array>
                                </map>
                                <map>
                                    <string key="leftOperand">jav:journal_article_version</string>
                                    <string key="operator">isAnyOf</string>
                                    <array key="rightOperand">
                                        <map><string key="@id">jav:VoR</string></map>
                                        <map><string key="@id">jav:AM</string></map>
                                        <map><string key="@id">jav:AO</string></map>
                                    </array>
                                </map>
                                <map>
                                    <string key="leftOperand">asf:displayable_element</string>
                                    <string key="operator">isAnyOf</string>
                                    <array key="rightOperand">
                                        <map><string key="@id">asf:citation</string></map>
                                    </array>
                                </map>
                            </array>
                        </map>
                    </xsl:if>
                    -->
                </array>
                <!-- TO DO: explicitly state what sharing activity is prohibited by a policy as the inverse of what the policy permits
                <array key="prohibition"/>
                -->
            </map>
        </xsl:variable>
        <xsl:sequence select="xml-to-json($data, map{'indent': false()})"/>
    </xsl:function>
    
    <!-- ** generate a table to visualize a policy at a glance -->
    <xsl:function name="asf:policy-glance-table" as="element(html:table)">
        <xsl:param name="policy" as="element(asf:policy)"/>
        <table class="asf-policy-glance pure-table pure-table-bordered">
            <tr>
                <th rowspan="2">Platform</th>
                <th rowspan="2">Article Version</th>
                <th colspan="4">Research Collaboration Group</th>
                <th colspan="4">General Access</th>
            </tr>
            <tr>
                <th>Citation</th>
                <th>Abstract</th>
                <th>References</th>
                <th>Full&#xA0;Text</th>
                <th>Citation</th>
                <th>Abstract</th>
                <th>References</th>
                <th>Full&#xA0;Text</th>
            </tr>
            <!-- ** create each table cell using the logic of what each term includes -->
            <tr>
                <th rowspan="3" scope="rowgroup"><abbr title="Platform that has signed and complies with the STM Voluntary Principles for Article Sharing">Signed</abbr></th>
                <th scope="row"><abbr title="Version of Record">VoR</abbr></th>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $vor and $p/@audience = ($rcg, $ga) 
                    and $p/@element = ($cm, $ab, $ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $vor and $p/@audience = ($rcg, $ga) 
                    and $p/@element = ($ab, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $vor and $p/@audience = ($rcg, $ga) 
                    and $p/@element = ($ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $vor and $p/@audience = ($rcg, $ga) 
                    and $p/@element = $ft))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $vor and $p/@audience = $ga 
                    and $p/@element = ($cm, $ab, $ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $vor and $p/@audience = $ga 
                    and $p/@element = ($ab, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $vor and $p/@audience = $ga 
                    and $p/@element = ($ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $vor and $p/@audience = $ga 
                    and $p/@element = $ft))"/>
            </tr>
            <tr>
                <th scope="row"><abbr title="Accepted Manuscript">AM</abbr></th>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $am and $p/@audience = ($rcg, $ga) 
                    and $p/@element = ($cm, $ab, $ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $am and $p/@audience = ($rcg, $ga) 
                    and $p/@element = ($ab, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $am and $p/@audience = ($rcg, $ga) 
                    and $p/@element = ($ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $am and $p/@audience = ($rcg, $ga) 
                    and $p/@element = $ft))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $am and $p/@audience = $ga 
                    and $p/@element = ($cm, $ab, $ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $am and $p/@audience = $ga 
                    and $p/@element = ($ab, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $am and $p/@audience = $ga 
                    and $p/@element = ($ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $am and $p/@audience = $ga 
                    and $p/@element = $ft))"/>
            </tr>
            <tr>
                <th scope="row"><abbr title="Author Original">AO</abbr></th>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $ao and $p/@audience = ($rcg, $ga) 
                    and $p/@element = ($cm, $ab, $ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $ao and $p/@audience = ($rcg, $ga) 
                    and $p/@element = ($ab, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $ao and $p/@audience = ($rcg, $ga) 
                    and $p/@element = ($ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $ao and $p/@audience = ($rcg, $ga) 
                    and $p/@element = $ft))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $ao and $p/@audience = $ga 
                    and $p/@element = ($cm, $ab, $ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $ao and $p/@audience = $ga 
                    and $p/@element = ($ab, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $ao and $p/@audience = $ga 
                    and $p/@element = ($ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = ($ps, $pns) and $p/@jav = $ao and $p/@audience = $ga 
                    and $p/@element = $ft))"/>
            </tr>
            <tr>
                <th rowspan="3" scope="rowgroup"><abbr title="Platform that has not signed or does not comply with the STM Voluntary Principles for Article Sharing">Not Signed</abbr></th>
                <th scope="row"><abbr title="Version of Record">VoR</abbr></th>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $vor and $p/@audience = ($rcg, $ga) 
                    and $p/@element = ($cm, $ab, $ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $vor and $p/@audience = ($rcg, $ga) 
                    and $p/@element = ($ab, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $vor and $p/@audience = ($rcg, $ga) 
                    and $p/@element = ($ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $vor and $p/@audience = ($rcg, $ga) 
                    and $p/@element = $ft))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $vor and $p/@audience = $ga 
                    and $p/@element = ($cm, $ab, $ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $vor and $p/@audience = $ga 
                    and $p/@element = ($ab, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $vor and $p/@audience = $ga 
                    and $p/@element = ($ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $vor and $p/@audience = $ga 
                    and $p/@element = $ft))"/>
            </tr>
            <tr>
                <th scope="row"><abbr title="Accepted Manuscript">AM</abbr></th>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $am and $p/@audience = ($rcg, $ga) 
                    and $p/@element = ($cm, $ab, $ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $am and $p/@audience = ($rcg, $ga) 
                    and $p/@element = ($ab, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $am and $p/@audience = ($rcg, $ga) 
                    and $p/@element = ($ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $am and $p/@audience = ($rcg, $ga) 
                    and $p/@element = $ft))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $am and $p/@audience = $ga 
                    and $p/@element = ($cm, $ab, $ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $am and $p/@audience = $ga 
                    and $p/@element = ($ab, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $am and $p/@audience = $ga 
                    and $p/@element = ($ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $am and $p/@audience = $ga 
                    and $p/@element = $ft))"/>
            </tr>
            <tr>
                <th scope="row"><abbr title="Author Original">AO</abbr></th>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $ao and $p/@audience = ($rcg, $ga) 
                    and $p/@element = ($cm, $ab, $ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $ao and $p/@audience = ($rcg, $ga) 
                    and $p/@element = ($ab, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $ao and $p/@audience = ($rcg, $ga) 
                    and $p/@element = ($ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $ao and $p/@audience = ($rcg, $ga) 
                    and $p/@element = $ft))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $ao and $p/@audience = $ga 
                    and $p/@element = ($cm, $ab, $ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $ao and $p/@audience = $ga 
                    and $p/@element = ($ab, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $ao and $p/@audience = $ga 
                    and $p/@element = ($ref, $ft)))"/>
                <xsl:sequence select="asf:table-cell-permit(some $p in ($policy/asf:permit) satisfies (
                    $p/@platform = $pns and $p/@jav = $ao and $p/@audience = $ga 
                    and $p/@element = $ft))"/>
            </tr>
            <tr>
                <td colspan="10"><span class="yes">Yes</span> = sharing is allowed, <span class="no">No</span> = sharing is not allowed</td>
            </tr>
        </table>
    </xsl:function>
    
    <!-- ** generate a table cell to visualize Yes or No -->
    <xsl:function name="asf:table-cell-permit" as="element(html:td)">
        <xsl:param name="permit" as="xs:boolean"/>
        <xsl:if test="$permit">
            <td class="yes">Yes</td>
        </xsl:if>
        <xsl:if test="not($permit)">
            <td class="no">No</td>
        </xsl:if>
    </xsl:function>
    
    <!-- ** CSS to include in the <head> of each HTML page -->
    <xsl:function name="asf:html-head-template" as="element()*" expand-text="no">
        <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.5/build/pure-min.css" integrity="sha384-LTIDeidl25h2dPxrB2Ekgc9c7sEC3CWGM6HeFmuDNUjX76Ert4Z4IY714dhZHPLd" crossorigin="anonymous"/>
        <style type="text/css">
            body { font-size: 1rem; line-height: 1.5; }
            #main { margin-left: 220px; margin-right: 20px; display: block; }
            #menu { width: 200px; position: fixed; background: #211433; color: #ffffff; overflow-y: auto; top: 0; bottom: 0; z-index: 1000; }
            a.pure-menu-link, span.pure-menu-heading { color: #ffffff; } 
            a.pure-menu-link:hover { background-color: #30A2FF; }
            li.pure-menu-selected { background-color: #aaaaaa; }
            table.asf-policy-glance { font-size: 0.8rem; }
            table.asf-policy-glance tr:nth-of-type(2) th { font-size: 0.7rem; white-space: nowrap; }
            table.asf-policy-glance td.yes, 
            table.asf-policy-glance span.yes { background-color: #BCF6D3; text-align: center; padding: 4px; }
            table.asf-policy-glance td.no, 
            table.asf-policy-glance span.no { background-color: #FF2F0F; text-align: center; padding: 4px; }
            table.asf-wordpress { margin-top: 2em; margin-bottom: 2em; font-size: 0.9rem; }
            table.asf-wordpress th { text-align: left; }
            table.asf-wordpress td { font-family: Consolas,Monaco,monospace; }
            table.asf-wordpress img { width: 13px; }
        </style>
        <script src="https://cdn.jsdelivr.net/npm/clipboard@2.0.8/dist/clipboard.min.js"></script>
        <script type="text/javascript">
            window.onload=function(){ 
                var clipboard = new ClipboardJS('.btn');
            }
        </script>
    </xsl:function>
    
    <!-- ** generate Crossref metadata deposit XML -->
    <xsl:function name="asf:crossref-xml" as="element(Q{http://www.crossref.org/schema/4.4.1}doi_batch)">
        <xsl:param name="policy" as="element(asf:policy)*"/>
        <xsl:variable name="timestamp" select="current-dateTime() 
            => adjust-dateTime-to-timezone(xs:dayTimeDuration('PT0H'))
            => format-dateTime('[Y][M01][D01][H01][m01][s01][f001]')"/>
        <doi_batch version="4.4.1" xmlns="http://www.crossref.org/schema/4.4.1"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://www.crossref.org/schema/4.4.1 http://data.crossref.org/schemas/crossref4.4.1.xsd">
            <head>
                <doi_batch_id>asf-{$timestamp}</doi_batch_id>
                <timestamp>{$timestamp}</timestamp>
                <depositor>
                    <depositor_name>International STM Association</depositor_name>
                    <email_address>dobrien@acs.org</email_address>
                </depositor>
                <registrant>International STM Association</registrant>
            </head>
            <body>
                <database>
                    <database_metadata>
                        <titles>
                            <title>Article Sharing Framework</title>
                        </titles>
                        <doi_data>
                            <doi>{asf:doi-id-without-url($asf-doi)}</doi>
                            <resource>{$url-prefix}</resource>
                        </doi_data>
                    </database_metadata>
                </database>
                <database>
                    <database_metadata>
                        <titles>
                            <title>Article Sharing Framework Vocabulary</title>
                        </titles>
                        <doi_data>
                            <doi>{asf:doi-id-without-url($vocab-doi)}</doi>
                            <resource>{$vocab-url}</resource>
                        </doi_data>
                    </database_metadata>
                </database>
                <database>
                    <database_metadata>
                        <titles>
                            <title>Article Sharing Framework Profile</title>
                        </titles>
                        <doi_data>
                            <doi>{asf:doi-id-without-url($odrl-profile-doi)}</doi>
                            <resource>{$url-prefix}</resource>
                        </doi_data>
                    </database_metadata>
                </database>
                <xsl:for-each select="$policy">
                    <database>
                        <database_metadata>
                            <titles>
                                <title>{asf:policy-title(@number)}</title>
                            </titles>
                            <doi_data>
                                <doi>{asf:policy-doi(@number) => asf:doi-id-without-url()}</doi>
                                <resource>{asf:policy-url(@number)}</resource>
                            </doi_data>
                        </database_metadata>
                    </database>
                </xsl:for-each>
            </body>
        </doi_batch>
    </xsl:function>
    
    <!-- ** remove URL prefix from a DOI -->
    <xsl:function name="asf:doi-id-without-url" as="xs:string">
        <xsl:param name="doi" as="xs:string"/>
        <xsl:sequence select="substring-after($doi, 'https://doi.org/')"/>
    </xsl:function>
    
    <!-- ** generate HTML page with information for Crossref DOI registration -->
    <xsl:function name="asf:crossref-page" as="element(html:html)">
        <xsl:param name="policy" as="element(asf:policy)*"/>
        <html lang="en" xml:lang="en">
            <head>
                <xsl:sequence select="asf:html-head-template()"/>
                <title>Article Sharing Framework Policies</title>
            </head>
            <body>
                <div id="layout">
                    <div id="menu">
                        <div class="pure-menu">
                            <ul class="pure-menu-list">
                                <li class="pure-menu-item">
                                    <a href="index.html" class="pure-menu-link">Back to list</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div id="main">
                        <article>
                            <h1>ASF Crossref DOIs</h1>
                            <p>This page lists the DOIs that should be registered with Crossref for each article sharing policy.</p>
                            <p>Download the Crossref metadata deposit XML file <a href="crossref.xml">crossref.xml</a>.</p>
                            <table class="pure-table pure-table-bordered">
                                <thead>
                                <tr>
                                    <th>Policy Page Title</th>
                                    <th>DOI</th>
                                    <th>URL</th>
                                </tr>
                                </thead>
                                <tr>
                                    <td>Article Sharing Framework</td>
                                    <td>{asf:doi-id-without-url($asf-doi)}</td>
                                    <td>{$url-prefix}</td>
                                </tr>
                                <tr>
                                    <td>Article Sharing Framework Vocabulary</td>
                                    <td>{asf:doi-id-without-url($vocab-doi)}</td>
                                    <td>{$vocab-url}</td>
                                </tr>
                                <tr>
                                    <td>Article Sharing Framework Profile</td>
                                    <td>{asf:doi-id-without-url($odrl-profile-doi)}</td>
                                    <td>{$url-prefix}</td>
                                </tr>
                                <xsl:for-each select="$policy">
                                    <tr>
                                        <td>{asf:policy-title(@number)}</td>
                                        <td>{asf:policy-doi(@number) => asf:doi-id-without-url()}</td>
                                        <td>{asf:policy-url(@number)}</td>
                                    </tr>
                                </xsl:for-each>
                            </table>
                        </article>
                    </div>
                </div>
            </body>
        </html>
    </xsl:function>
    
    <!-- ** create links to current, previous, and next version of a sharing policy 
            Version numbers were removed from URLs, but this logit to generate links 
            is still present as is the logic to insert dc:replaces and dc:isReplacedBy 
            The logic could be used again if there is a need for linking older/newer versions
            which would probably be a new policy number replacing an older policy number. 
    -->
    <xsl:function name="asf:versioning-links" as="element(html:p)*">
        <xsl:param name="policy" as="element(asf:policy)"/>
        <p>This version: <a href="{asf:policy-url($policy/@number)}">{asf:policy-url($policy/@number)}</a></p>
        <xsl:if test="$policy/@replaces_version">
            <p>Previous version: <a href="{asf:policy-url($policy/@number)}">{asf:policy-url($policy/@number)}</a></p>
        </xsl:if>
        <xsl:if test="$policy/@replaced_by_version">
            <p>Newer version: <a href="{asf:policy-url($policy/@number)}">{asf:policy-url($policy/@number)}</a></p>
        </xsl:if>
    </xsl:function>
    
    <!-- ** generate the HTML page for a policy including English text, metadata, visualization, and details -->
    <xsl:function name="asf:policy-page" as="element(html:html)">
        <xsl:param name="policy" as="element(asf:policy)"/>
        <html lang="en" xml:lang="en">
            <head>
                <xsl:sequence select="asf:html-head-template()"/>
                <title>{asf:policy-title($policy/@number)}</title>
            </head>
            <body>
                <div id="layout">
                    <div id="menu">
                        <div class="pure-menu">
                            <ul class="pure-menu-list">
                                <li class="pure-menu-item">
                                    <a href="index.html" class="pure-menu-link">Back to list</a>
                                </li>
                                <li class="pure-menu-item">
                                    <a href="wordpress.html#{asf:policy-anchor($policy/@number, $policy/@policy_version)}" class="pure-menu-link">WordPress HTML</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div id="main">
                        <article>
                            <h2>{asf:policy-title($policy/@number)}</h2>
                            <xsl:sequence select="asf:policy-statement($policy)"/>
                            <p>DOI: <a href="{asf:policy-doi($policy/@number)}">{asf:policy-doi($policy/@number)}</a></p>
                            <p>Abbreviation: {asf:abbreviation($policy)}</p>
                            <script type="application/ld+json">{asf:policy-metadata($policy)}</script>
                        </article>
                    </div>
                </div>
            </body>
        </html>
    </xsl:function>
    
    <!-- ** generate policy HTML for WordPress to make copy/past easier -->
    <xsl:function name="asf:wordpress-policy" as="element(html:article)">
        <xsl:param name="policy" as="element(asf:policy)"/>
        <xsl:variable name="anchor" select="asf:policy-anchor($policy/@number, $policy/@policy_version)"/>
        <article id="{$anchor}">
            <h1><a href="{asf:policy-filename($policy/@number, $policy/@policy_version)}">Policy {$policy/@number}</a></h1>
            <table class="asf-wordpress pure-table pure-table-bordered">
                <tr>
                    <th>Title</th>
                    <td id="{$anchor}_title">{asf:policy-title($policy/@number)}</td>
                    <td><xsl:sequence select="asf:wordpress-copy-button($anchor||'_title')"/></td>
                </tr>
                <tr>
                    <th>Permalink</th>
                    <td id="{$anchor}_permalink">{asf:policy-url($policy/@number)}</td>
                    <td><xsl:sequence select="asf:wordpress-copy-button($anchor||'_permalink')"/></td>
                </tr>
                <tr>
                    <th>Permalink Slug</th>
                    <td id="{$anchor}_slug">{asf:policy-url($policy/@number)
                        => substring-after($url-prefix)
                        => substring-before('/')
                    }</td>
                    <td><xsl:sequence select="asf:wordpress-copy-button($anchor||'_slug')"/></td>
                </tr>
                <tr>
                    <th>HTML Text</th>
                    <td id="{$anchor}_text">
                        <xsl:variable name="content" select="
                            asf:policy-page($policy)//html:article/* ! 
                            (if (position() eq last()) then . else (., '&#xF0000;'))"/>
                        <xsl:variable name="text" select="
                            serialize($content, map{'method': 'html', 'indent': false()} )
                            => replace(' xmlns:html=&quot;http://www.w3.org/1999/xhtml&quot;', '')
                            => replace(' xmlns=&quot;http://www.w3.org/1999/xhtml&quot;', '')
                            "/>
                        <xsl:for-each select="tokenize($text, '&#xF0000;')">
                            {.}<br/><br/>
                        </xsl:for-each>
                    </td>
                    <td><xsl:sequence select="asf:wordpress-copy-button($anchor||'_text')"/></td>
                </tr>
            </table>
        </article>
    </xsl:function>
    
    <!-- ** generate button to copy text to clipboard for the WordPress page -->
    <xsl:function name="asf:wordpress-copy-button" as="element(html:button)">
        <xsl:param name="target" as="xs:string"/>
        <button class="btn" data-clipboard-target="#{$target}" title="Copy to clipboard">
            <img src="https://clipboardjs.com/assets/images/clippy.svg" alt="Copy to clipboard"/>
        </button>
    </xsl:function>
    
    <!-- ** generate page listing all policies for copy/paste into WordPress -->
    <xsl:function name="asf:wordpress-page" as="element(html:html)">
        <xsl:param name="policies" as="element(asf:policy)*"/>
        <html lang="en" xml:lang="en">
            <head>
                <xsl:sequence select="asf:html-head-template()"/>
                <title>Article Sharing Framework Policies</title>
            </head>
            <body>
                <div id="layout">
                    <div id="menu">
                        <div class="pure-menu">
                            <ul class="pure-menu-list">
                                <li class="pure-menu-item">
                                    <a href="index.html" class="pure-menu-link">Back to list</a>
                                </li>
                                <li class="pure-menu-item">
                                    <span class="pure-menu-heading">Policies</span>
                                </li>
                                <xsl:for-each select="$policies">
                                    <li class="pure-menu-item">
                                        <a href="#{asf:policy-anchor(@number, @policy_version)}" class="pure-menu-link">Policy {@number}</a>
                                    </li>
                                </xsl:for-each>
                            </ul>
                        </div>
                    </div>
                    <div id="main">
                        <h1>ASF WordPress HTML</h1>
                        <p>This page lists all of the sharing policies in a format that can be easily copy/pasted into WordPress to create a page for each sharing policy on the STM website.
                        In the WordPress page editor be sure to click the "Text" tab before pasting the HTML Text into WordPress. If the "Visual" tab is active the HTML Text will not be pasted correctly.</p>
                        <xsl:sequence select="for $policy in $policies return asf:wordpress-policy($policy)"/>
                    </div>
                </div>
            </body>
        </html>
    </xsl:function>
    
    <!-- ** generate an HTML page that lists all of the policies -->
    <xsl:function name="asf:index-page" as="element(html:html)">
        <xsl:param name="policies" as="element(asf:policy)*"/>
        <xsl:param name="menu-index" as="xs:integer"/>
        <xsl:param name="subsets" as="element(asf:subset)*"/>
        <xsl:variable name="url-about" select="'https://gitlab.com/vincentml/asf#asf-sharing-policy-generator'"/>
        <xsl:variable name="url-about-metadata" select="'https://gitlab.com/vincentml/asf#sharing-policy-metadata'"/>
        <html lang="en" xml:lang="en">
            <head>
                <xsl:sequence select="asf:html-head-template()"/>
                <title>Article Sharing Framework Policies</title>
            </head>
            <body>
                <div id="layout">
                    <div id="menu">
                        <div class="pure-menu">
                            <ul class="pure-menu-list">
                                <li class="pure-menu-item">
                                    <a href="{$url-prefix}" class="pure-menu-link">ASF</a>
                                </li>
                                <li class="pure-menu-item">
                                    <a href="{$url-about}" class="pure-menu-link">About</a>
                                </li>
                                <li class="pure-menu-item">
                                    <a href="crossref.html" class="pure-menu-link">Crossref DOIs</a>
                                </li>
                                <li class="pure-menu-item">
                                    <a href="wordpress.html" class="pure-menu-link">WordPress HTML</a>
                                </li>
                                <li class="pure-menu-item">
                                    <span class="pure-menu-heading">Filters</span>
                                </li>
                                <li class="pure-menu-item{if ($menu-index = 1) then ' pure-menu-selected' else ()}">
                                    <a href="index.html" class="pure-menu-link">All</a>
                                </li>
                                <xsl:for-each select="$subsets">
                                    <li class="pure-menu-item{if ($menu-index = 1 + number(@number)) then ' pure-menu-selected' else ()}">
                                        <a href="index-subset{@number}.html" class="pure-menu-link">Subset {@number}</a>
                                    </li>
                                </xsl:for-each>
                                <li class="pure-menu-item">
                                    <span class="pure-menu-heading">Policies{
                                        if ($menu-index = 1) then '' else ' Subset ' || $menu-index - 1}</span>
                                </li>
                                <xsl:for-each select="$policies">
                                    <li class="pure-menu-item">
                                        <a href="#{asf:policy-anchor(@number, @policy_version)}" class="pure-menu-link">Policy {@number}</a>
                                    </li>
                                </xsl:for-each>
                            </ul>
                        </div>
                    </div>
                    <div id="main">
                        <h1>Article Sharing Framework Policies</h1>
                        <p>See the <a href="{$url-prefix}">STM's ASF page</a> for a description of the article sharing framework,
                            and see the <a href="{$url-about}">About page</a> for a description of the sharing policy <a href="{$url-about-metadata}">metadata</a> and the tool that generates these sharing policy pages.
                            The <a href="crossref.html">Crossref DOIs</a> page lists the DOIs that should be registered with Crossref and provides a metadata deposit XML file.
                            The <a href="wordpress.html">WordPress HTML</a> page provides the sharing policies formatted to easily copy/paste into WordPress in order to create sharing policy pages on the STM website.
                        </p>
                        <p>Updated: {format-date($modified_date, '[Y]-[M01]-[D01]')}</p>
                        <p>This is a list of the sharing policies{ if ($menu-index = 1) then '' else ' in subset ' || $menu-index - 1}. Click on any policy heading for the individual sharing policy page.</p>
                        <xsl:for-each select="$policies">
                            <article id="{asf:policy-anchor(@number, @policy_version)}">
                                <h1><a href="{asf:policy-filename(@number, @policy_version)}">Policy {@number}</a></h1>
                                <h2>{asf:policy-title(./@number)}</h2>
                                <xsl:sequence select="asf:policy-statement(.)"/>
                                <p>DOI: <a href="{asf:policy-doi(./@number)}">{asf:policy-doi(./@number)}</a></p>
                                <xsl:sequence select="asf:versioning-links(.)"/>
                                <p>Abbreviation: {asf:abbreviation(.)}</p>
                                <script type="application/ld+json">{asf:policy-metadata(.)}</script>
                                <xsl:sequence select="asf:policy-glance-table(.)"/>
                            </article>
                        </xsl:for-each>
                    </div>
                </div>
            </body>
        </html>
    </xsl:function>
    
    <!-- ** output all of the HTML pages -->
    <xsl:template name="asf:generate-pages">
        <xsl:param name="policies" as="element(asf:policy)*"/>
        <xsl:param name="subsets" as="element(asf:subset)*"/>
        <xsl:for-each select="$policies">
            <xsl:result-document href="{concat($destination, asf:policy-filename(@number, @policy_version))}" format="html5">
                <xsl:sequence select="asf:policy-page(.)"/>
            </xsl:result-document>
        </xsl:for-each>
        <xsl:result-document href="{concat($destination, 'index.html')}" format="html5">
            <xsl:sequence select="asf:index-page($policies, 1, $subsets)"/>
        </xsl:result-document>
        <xsl:for-each select="$subsets">
            <xsl:variable name="subset" select="." as="element(asf:subset)"/>
            <xsl:result-document href="{concat($destination, 'index-subset', $subset/@number, '.html')}" format="html5">
                <xsl:sequence select="asf:index-page($policies[@number = $subset/asf:policy_ref/@number], 1 + xs:integer($subset/@number), $subsets)"/>
            </xsl:result-document>
        </xsl:for-each>
        <xsl:variable name="crossref-urls" as="element(asf:policy)*" select="$policies[not(@replaced_by_version)][not(@number = 999)]"/>
        <xsl:result-document href="{concat($destination, 'crossref.html')}" format="html5">
            <xsl:sequence select="asf:crossref-page($crossref-urls)"/>
        </xsl:result-document>
        <xsl:result-document href="{concat($destination, 'crossref.xml')}" method="xml" indent="yes">
            <xsl:sequence select="asf:crossref-xml($crossref-urls)"/>
        </xsl:result-document>
        <xsl:result-document href="{concat($destination, 'wordpress.html')}" format="html5">
            <xsl:sequence select="asf:wordpress-page($crossref-urls)"/>
        </xsl:result-document>
    </xsl:template>
    
</xsl:stylesheet>