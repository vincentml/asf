<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2"
    xmlns:asf="https://doi.org/10.15223/asf-vocabulary#"
    xmlns:sqf="http://www.schematron-quickfix.com/validator/process">
    
    <sch:ns prefix="asf" uri="https://doi.org/10.15223/asf-vocabulary#"/>
    
    <sch:pattern>
        <sch:rule context="asf:policy">
            <sch:let name="a_policy" value="."/>
            <sch:report id="policy_001" role="error"
                test="some $b_policy in (../asf:policy[@number ne $a_policy/@number]) satisfies (
                count($a_policy/asf:permit) eq count($b_policy/asf:permit)
                and (every $a_permit in ($a_policy/asf:permit) satisfies (
                    some $b_permit in ($b_policy/asf:permit) satisfies deep-equal($a_permit, $b_permit)))  
                )">policy <sch:value-of select="@number"/> provides same permissions as another policy</sch:report>
            
            <sch:assert id="policy_002" role="error"
                test="asf:permit[2] or number(@number) gt 900"
                >policy has only one permit and probably duplicates one of the 48 permutations</sch:assert>
            
            <sch:assert id="policy_003" role="error"
                test="not(preceding::asf:policy)
                or (number(@number) eq number(preceding::asf:policy[1]/@number) and number(@policy_version) gt number(preceding::asf:policy[1]/@policy_version))
                or (number(@number) eq number(preceding::asf:policy[1]/@number) + 1)
                or (number(@number) ge 900 and @policy_version eq '1.0')"
                >policy <sch:value-of select="@number"/> version <sch:value-of select="@policy_version"/> is not in numerical order</sch:assert>
            
            <sch:assert id="policy_004" role="error" 
                test="not(@replaced_by_version) 
                or ../asf:policy[@number eq $a_policy/@number][@policy_version eq $a_policy/@replaced_by_version]"
                >policy <sch:value-of select="@number"/> replaced by version <sch:value-of select="@replaced_by_version"/> is not defined</sch:assert>
            
            <sch:assert id="policy_005" role="error"
                test="not(@replaces_version)
                or ../asf:policy[@number eq $a_policy/@number][@policy_version eq $a_policy/@replaces_version]
                or (number(@number) le 48 and @replaces_version eq '1.0')"
                >policy <sch:value-of select="@number"/> replaces version <sch:value-of select="@replaces_version"/> is not defined</sch:assert>
            
            <sch:assert id="policy_006" role="error" 
                test="not(@replaces_version) or number(@replaces_version) lt number(@policy_version)"
                >policy <sch:value-of select="@number"/> replaces version <sch:value-of select="@replaces_version "/> shold be lower than version <sch:value-of select="@policy_version"/></sch:assert>
            
            <sch:assert id="policy_007" role="error" 
                test="not(@replaced_by_version) or number(@replaced_by_version) gt number(@policy_version)"
                >policy <sch:value-of select="@number"/> replaced by version <sch:value-of select="@replaced_by_version "/> shold be higher than version <sch:value-of select="@policy_version"/></sch:assert>
            
        </sch:rule>
        
        <sch:rule context="asf:subset">
            <sch:assert id="subset_001" role="error" 
                test="number(@number) eq count(preceding::asf:subset) + 1"
                >subset <sch:value-of select="@number"/> is not in numerical order</sch:assert>
        </sch:rule>
        
        <sch:rule context="asf:policy_ref">
            <sch:assert id="policy_ref_001" role="error" 
                test="(number(@number) le 48 and number(@number) ge 1) or @number = preceding::asf:policy/@number"
                >policy <sch:value-of select="@number"/> is not defined</sch:assert>
        </sch:rule>
        
    </sch:pattern>
    
</sch:schema>
